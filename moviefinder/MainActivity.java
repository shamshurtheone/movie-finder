package com.shamshur.moviefinder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.URL;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.korean).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseKorean("https://www1.mydramaoppa.com/movies/");

            }

        });
        findViewById(R.id.hollywood).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseHollywood("https://ww7.123moviesfree.sc/");

            }

        });
        findViewById(R.id.bollywood).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseBollywood("https://www1.mydramaoppa.com/genre/indian/");

            }

        });

    }



    //public void ke baad hamesha clickOnlistener ka id dena hai
    public void browseKorean(String url){
        Intent intent=new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    public void browseHollywood(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    public void browseBollywood(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}



